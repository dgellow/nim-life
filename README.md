Travis CI: [![Build Status](https://travis-ci.org/dgellow/life.svg?branch=master)](https://travis-ci.org/dgellow/life)
GitLab CI: [![build status](https://gitlab.com/ci/projects/17481/status.png?ref=master)](https://gitlab.com/ci/projects/17481?ref=master)

# Some fun with [Nim](http://nim-lang.org/)

![](http://zippy.gfycat.com/DefensiveLeftBedbug.gif)

## Build and run

```
git clone https://github.com/dgellow/life.git
cd life
nim c -r life.nim
```
