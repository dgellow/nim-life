from os import sleep
from times import epochTime
from strutils import `%`
from sequtils import newSeqWith, filter
import unsigned

import libncurses

type
  Color = cshort

const
  speed = 100 # miliseconds
  charAlive = "0"
  charDead = "_"
  colorDefault: Color = 2
  colorMin: Color = 1
  colorMax: Color = 8

var
  window: ptr WINDOW
  nbRow: int
  nbCol: int

type
  Cell = tuple
    x: int
    y: int
    alive: bool

type
  Row = seq[int]

type
  Board = seq[Row]

type
  Neighbours = array[8, Cell]

proc getCell(cells: Board, x: int, y: int): Cell
proc draw(cells: Board, color: Color = colorDefault): Board

iterator items(b: Board): Cell =
  for y in 0..(nbRow - 1):
    for x in 0..(nbCol - 1):
      yield b.getCell(x, y)

iterator deltat(): int {.closure.} =
  var
    t0: float = epochTime()
    t1: float
  while true:
    t1 = epochTime()
    yield ((t1 - t0) * 1000).toInt()
    t0 = t1

iterator nextframe(): bool {.closure.} =
  let
    genDeltaT = deltat
  var
    accTime: int = 0
  while true:
    accTime += genDeltaT()
    if accTime >= speed:
      yield true
      accTime = 0
    else:
      yield false

iterator colors(): Color {.closure.} =
  var
    color: Color = colorDefault
    counter_loop = 0

  while true:
    if counter_loop mod 20 == 0 and color < colorMax:
      color += 1
    elif color >= colorMax:
      color = colorMin
      counter_loop = 0
    counter_loop += 1
    yield color

converter toSeq[I, T](a: array[I, T]): seq[T] =
  a.map(proc (x: T): T = x)

proc initialize(): void =
  window = initscr()
  nbRow = getmaxx(window)
  nbCol = getmaxx(window)
  discard cbreak()
  discard noecho()
  discard start_color()
  discard keypad(stdscr, true)
  discard nodelay(stdscr, true)
  discard mousemask(ALL_MOUSE_EVENTS, nil)

  discard init_pair(100, COLOR_BLACK, COLOR_BLACK);
  for c in colorMin..colorMax:
    discard init_pair(c + 1, c, c);

proc terminate(): void {.noconv.} =
  discard endwin()
  quit()

proc kbhit(): bool =
  let
    ch: cint = wgetch(window)
  if ch == ERR:
    false
  else:
    discard ungetch(ch)
    true

proc putCharAt(x: int, y:int, ch: string, color: Color): void =
  let
    x_c = cast[cint](x)
    y_c = cast[cint](y)

  if ch == charAlive:
    discard attron(COLOR_PAIR(color + 1));
    discard mvaddstr(y_c, x_c, ch)
    discard attroff(COLOR_PAIR(color + 1));
  else:
    discard attron(COLOR_PAIR(100));
    discard mvaddstr(y_c, x_c, ch)
    discard attroff(COLOR_PAIR(100));

proc createBoard(): Board =
  newSeqWith(nbRow, newSeq[int](nbCol))

proc setSeed(board: var Board): Board =
  var
    event: mevent

  while true:
    if not kbhit():
      continue

    case wgetch(window)
    of KEY_MOUSE:
      if (getmouse(addr event) == OK) and
         ((event.bstate and cast[cuint](BUTTON1_CLICKED)) != 0):
          let
            cell = board.getCell(event.x, event.y)
          board[event.y][event.x] = if cell.alive: 0 else: 1
          discard board.draw()

          discard mvaddstr(5, 7, ("x: $1, y: $2, z: $3" % [$event.x, $event.y, $event.z]))
          discard mvaddstr(6, 7, $board.getCell(event.x, event.y))
          discard refresh()
    of 10: # ENTER
      break
    else: discard
  return board


proc neighbourTop(cells: Board, c: Cell): Cell =
  cells.getCell(c.x, if c.y == 0: nbRow - 1
                     else: c.y - 1)

proc neighbourBottom(cells: Board, c: Cell): Cell =
  cells.getCell(c.x, if c.y == nbRow - 1: 0
                     else: c.y + 1)

proc neighbourLeft(cells: Board, c: Cell): Cell =
  cells.getCell(if c.x == 0: nbCol - 1
                else: c.x - 1,
                c.y)

proc neighbourRight(cells: Board, c: Cell): Cell =
  cells.getCell(if c.x == nbCol - 1: 0
                else: c.x + 1,
                c.y)

proc neighbourTopLeft(cells: Board, c: Cell): Cell =
  cells.getCell(if c.x == 0: nbCol - 1
                else: c.x - 1,
                if c.y == 0: nbRow - 1
                else: c.y - 1)

proc neighbourTopRight(cells: Board, c: Cell): Cell =
  cells.getCell(if c.x == nbCol - 1: 0
                else: c.x + 1,
                if c.y == 0: nbRow - 1
                else: c.y - 1)

proc neighbourBottomLeft(cells: Board, c: Cell): Cell =
  cells.getCell(if c.x == 0: nbCol - 1
                else: c.x - 1,
                if c.y == nbRow - 1: 0
                else: c.y + 1)

proc neighbourBottomRight(cells: Board, c: Cell): Cell =
  cells.getCell(if c.x == nbCol - 1: 0
                else: c.x + 1,
                if c.y == nbRow - 1: 0
                else: c.y + 1)

proc neighbours(c: Cell, cells: Board): Neighbours =
  [neighbourTop(cells, c),
   neighbourBottom(cells, c),
   neighbourLeft(cells, c),
   neighbourRight(cells, c),
   neighbourTopLeft(cells, c),
   neighbourTopRight(cells, c),
   neighbourBottomLeft(cells, c),
   neighbourBottomRight(cells, c)]

proc aliveNeighbours(c: Cell, cells: Board): int =
  c.neighbours(cells)
    .filter(proc (n: Cell): bool = n.alive)
    .len()

proc isUnderpopulated(c: Cell, aliveNeighbours: int): bool =
  aliveNeighbours < 2

proc willSurvive(c: Cell, aliveNeighbours: int): bool =
  c.alive and ((aliveNeighbours == 2) or (aliveNeighbours == 3))

proc isOvercrowded(c: Cell, aliveNeighbours: int): bool =
  aliveNeighbours > 3

proc willBecomeAlive(c: Cell, aliveNeighbours: int): bool =
  aliveNeighbours == 3

proc getCell(cells: Board, x: int, y: int): Cell =
  (x: x, y: y, alive: cells[y][x] == 1)

proc draw(c: Cell, color: Color): void =
  putCharAt(c.x, c.y, if c.alive: charAlive
                      else: charDead,
                      color)

proc draw(cells: Board, color = colorDefault): Board =
  for cell in cells:
    cell.draw(color)
  discard refresh()
  return cells

proc applyRules(cells: Board): Board =
  # Rule 1. Any live cell with fewer than two live neighbours dies,
  # as if caused by under-population.
  #
  # Rule 2. Any live cell with two or three live neighbours lives on
  # to the next generation.
  #
  # Rule 3. Any live cell with more than three live neighbours dies,
  # as if by overcrowding.
  #
  # Rule 4. Any dead cell with exactly three live neighbours becomes
  # a live cell, as if by reproduction.
  var
    n_cells: Board = createBoard()

  for cell in cells:
    let
      alive_n = cell.aliveNeighbours(cells)

    var
      cell_state: range[0..1]

    if cell.isUnderpopulated(alive_n):
      cell_state = 0
    elif cell.willSurvive(alive_n):
      cell_state = 1
    elif cell.isOvercrowded(alive_n):
      cell_state = 0
    elif cell.willBecomeAlive(alive_n):
      cell_state = 1

    n_cells[cell.y][cell.x] = cell_state

  return n_cells


proc gameloop(cells: Board): void =
  var
    n_cells = cells
  let
    genNextFrame = nextframe
    iterColors = colors

  while true:
    if kbhit():
      case wgetch(window)
      of 'r'.ord():
        return
      of 'q'.ord():
        terminate()
      else: discard

    if genNextFrame():
      n_cells = applyRules(n_cells)
      discard n_cells.draw(iterColors())
    sleep(1)
#
# Main
#

# Handle SIGINT
setControlCHook(terminate)

while true:
  initialize()
  var
    board = createBoard().draw()
  gameloop(board.setSeed())
