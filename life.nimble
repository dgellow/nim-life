[Package]
name          = "game-of-life"
version       = "0.0.1"
author        = "Samuel El-Borai"
description   = "Conward's game of life"
license       = "MIT"

[Deps]
Requires: "nim >= 0.10.0"
